package com.ubiquime.model;

public class AppException extends Exception {
	private static final long serialVersionUID = 1L;

	public AppException() {
	}

	public AppException(String detailMessage) {
		super(detailMessage);
	}

	public AppException(Throwable throwable) {
		super(throwable);
	}

	public AppException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}

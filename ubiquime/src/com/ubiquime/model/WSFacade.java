package com.ubiquime.model;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.content.ContentResolver;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class WSFacade {
	public static final boolean useLocalHost = false;
	public static String wsHost = "rafael.pyboys.com";
	public static int wsPort = 80;
	public static final String wsPath = "/ubiquime/api/";
	public static final String wsSufix = ".json";
	
	public static void setup(ContentResolver contentResolver) {
		if (useLocalHost) {
			wsPort = 8000;
			if ("deviceIdString".equals(Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID))) {
				wsHost = "192.168.25.20";
			} else {
				wsHost = "10.0.2.2";
			}
		}
	}
	
	public static <Type> Type requestAction(String action, Map<String, String> parameters, TypeToken<Type> returnType)
			throws AppException {
		final String URL = createURL(action);
		Builder uriBuilder = Uri.parse(URL).buildUpon();
		for (Entry<String, String> parametro : parameters.entrySet()) {
			uriBuilder.appendQueryParameter(parametro.getKey(), parametro.getValue());
		}

		HttpClient client = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(uriBuilder.toString());
		getRequest.setHeader(HTTP.CONTENT_TYPE, "application/json");

		try {
			HttpResponse response = client.execute(getRequest);
			int responseStatusCode = response.getStatusLine().getStatusCode();
			if (responseStatusCode != HttpStatus.SC_OK) {
				Log.w(Application.class.getSimpleName(), "Error " + responseStatusCode + " for URL " + URL);
				throw new IOException("Connection error.");
			}

//			//Traditional JSON:
//			String resultStr = EntityUtils.toString(response.getEntity());
//			JSONTokener jsonTokener = new JSONTokener(resultStr);
//			if (jsonTokener.more()) {
//				return (JSONObject) jsonTokener.nextValue();
//			}
			// Conversion to object with Gson:
			Reader jsonReader = new InputStreamReader(response.getEntity().getContent());
			return new Gson().fromJson(jsonReader, returnType.getType());
		} catch (Exception e) {
			getRequest.abort();
			Log.e(Application.class.getName(), "Connection error", e);
			throw new AppException("Connection error", e);
		}
	}
	
	private static String createURL(String action) {
		try {
			return new URL("http", wsHost, wsPort, wsPath).toURI().resolve(action + wsSufix).toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

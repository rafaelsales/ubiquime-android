package com.ubiquime.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@SerializedName("id")
	public long id;
	
	@SerializedName("name")
	public String name;
	
	@SerializedName("identifier")
	public String identifier;
	
	@SerializedName("distance")
	public int distance;
	
	@SerializedName("locationName")
	public String locationName;
	
	@SerializedName("lat")
	public float lat;
	
	@SerializedName("lon")
	public float lon;

	public static List<Application> getNear(Double lat, Double lon) throws AppException {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("lat", lat.toString());
		parameters.put("lon", lon.toString());
		return WSFacade.requestAction("getApplications", parameters, new TypeToken<List<Application>>() {
		});
	}

}

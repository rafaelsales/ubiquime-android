package com.ubiquime.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.SystemService;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;
import com.ubiquime.model.AppException;
import com.ubiquime.model.Application;
import com.ubiquime.model.WSFacade;

@EActivity(R.layout.applications)
public class UbiquimeAct extends SherlockActivity implements LocationListener {

	@SystemService
	LocationManager locationManager;

	@ViewById
	ListView listView;
	MenuItem menuRefresh;
	MenuItem menuMyLocation;

	Location myLocation = null;
	List<Application> apps = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		WSFacade.setup(getContentResolver());
	}

	@AfterViews
	void afterViews() {
	}

	@SuppressWarnings("rawtypes")
	@ItemClick(R.id.listView)
	void applicationSelected(Map selected) {
		Application app = (Application) selected.get("value");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("market://details?id=" + app.identifier));
		startActivity(intent);
	}

	void setRefreshingState(boolean enable) {
		menuRefresh.setVisible(!enable);
		menuMyLocation.setEnabled(!enable);
		setSupportProgressBarIndeterminateVisibility(enable);
	}

	/**
	 * Request a location update to Location Manager
	 */
	void requestRefreshLocation() {
		if (!isNetworkAvailable(this)) {
			Toast.makeText(this, "Network connection unavailable. Please check your settings or internet coverage.", Toast.LENGTH_LONG).show();
			return;
		}
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			setRefreshingState(true);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		} else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			setRefreshingState(true);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		} else {
			Toast.makeText(this, "Please, enable the GPS or Network location service.", Toast.LENGTH_LONG).show();
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null
		// otherwise check if we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	/**
	 * Search for applications near by the current location returned by the Location Manager
	 * 
	 * @param location
	 */
	@Background
	void refreshList(Location location) {
		Exception exception = null;
		try {
			apps = Application.getNear(location.getLatitude(), location.getLongitude());
		} catch (AppException e) {
			exception = e;
			e.printStackTrace();
		}
		onRefreshListResponse(apps, exception);
	}

	/**
	 * Update the UI with the application list returned by the server
	 * 
	 * @param apps
	 * @param exception
	 *            - if the request failed, the exception thrown is passed, otherwise null is passed
	 */
	@UiThread
	void onRefreshListResponse(List<Application> apps, Exception exception) {
		setRefreshingState(false);

		if (exception != null) {
			Toast.makeText(UbiquimeAct.this, exception.getMessage(), Toast.LENGTH_LONG).show();
			menuMyLocation.setEnabled(false);
		} else {
			ArrayList<HashMap<String, Object>> appsListViewItems = new ArrayList<HashMap<String, Object>>();
			for (Application app : apps) {
				HashMap<String, Object> appListViewItem = new HashMap<String, Object>();
				appListViewItem.put("value", app);
				appListViewItem.put("name", app.name);
				appListViewItem.put("distance", app.distance + " meters from " + app.locationName);
				appsListViewItems.add(appListViewItem);
			}
			SimpleAdapter appsListAdapter = new SimpleAdapter(UbiquimeAct.this, appsListViewItems,
					R.layout.app_list_item, new String[] { "name", "distance" }, new int[] { R.id.list_item_name,
							R.id.list_item_distance });

			listView.setAdapter(appsListAdapter);
		}
	}

	void showMap() {
		Intent intent = new Intent(UbiquimeAct.this, MapAct_.class);
		intent.putExtra("apps", (Serializable) apps);
		intent.putExtra("location", myLocation);
		startActivity(intent);
	}

	@Override
	public void onLocationChanged(Location location) {
		this.myLocation = location;
		setRefreshingState(true);
		refreshList(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menuRefresh = menu.add("Refresh");
		menuRefresh.setIcon(R.drawable.navigation_refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		menuMyLocation = menu.add("My Location");
		menuMyLocation.setIcon(R.drawable.location_place).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// TODO Remove:
		Location fakeLocationPici = new Location(LocationManager.NETWORK_PROVIDER);
		fakeLocationPici.setLatitude(-3.745967);
		fakeLocationPici.setLongitude(-38.575029);
		onLocationChanged(fakeLocationPici);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == menuRefresh) {
			requestRefreshLocation();
		} else if (item == menuMyLocation) {
			showMap();
		}
		return super.onOptionsItemSelected(item);
	}
}
package com.ubiquime.view;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.location.Location;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockMapActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ViewById;
import com.ubiquime.model.Application;

@EActivity(R.layout.map)
public class MapAct extends SherlockMapActivity {
	@ViewById
	MapView map;

	@Extra("location")
	Location myLocation;
	@Extra("apps")
	List<Application> apps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@AfterViews
	void afterViews() {
		map.getController().animateTo(convertLocationToGeoPoint(myLocation));
		map.setBuiltInZoomControls(true);
		map.setSatellite(false);
		map.getController().setZoom(18);
		map.getOverlays().add(
				new ImageOverlay(convertLocationToGeoPoint(myLocation), R.drawable.location_place));
		for (Application app : apps) {
			map.getOverlays().add(new ImageOverlay(convertDegreesToGeoPoint(app.lat, app.lon), R.drawable.ic_launcher));
		}
		map.invalidate();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class ImageOverlay extends Overlay {
		final GeoPoint geoPoint;
		final int resIdImage;
		final Paint paint = new Paint();

		public ImageOverlay(GeoPoint point, int resId) {
			this.geoPoint = point;
			this.resIdImage = resId;
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			Point point = mapView.getProjection().toPixels(geoPoint, null);
			Bitmap bmp = BitmapFactory.decodeResource(mapView.getResources(), resIdImage);
			RectF imageRect = new RectF(point.x, point.y, point.x + 48, point.y + 48);
			canvas.drawBitmap(bmp, null, imageRect, paint);
		}
	}

	GeoPoint convertLocationToGeoPoint(Location location) {
		return new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));
	}

	GeoPoint convertDegreesToGeoPoint(float latitude, float longitude) {
		return new GeoPoint((int) (latitude * 1E6), (int) (longitude * 1E6));
	}
}
